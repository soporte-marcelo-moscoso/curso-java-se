
public class Alumno extends Persona {

    private String matricula;

    public Alumno(String nombre,int edad, String matricula){
        // nonstraemos las variables del la clase padre
        super(nombre,edad);
        this.matricula = matricula;
    }

    @Override
    public String obtenerInformacion() {
      return "Nombre" + getNombre() + ", Edad " + getEdad() + "matricula" + matricula;
    }

    public String getMatricula(){
        return matricula;
    }

}