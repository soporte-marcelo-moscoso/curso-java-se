public class Profesor extends Persona {

    private String numeroEmpleado;


    public Profesor(String nombre,int edad, String numeroEmpleado){
        super(nombre,edad);
        this.numeroEmpleado = numeroEmpleado;
    }

    public String getNumeroEmpleado() {
        return numeroEmpleado;
    }



    public void setNumeroEmpleado(String numeroEmpleado) {
        this.numeroEmpleado = numeroEmpleado;
    }



    @Override
    public String obtenerInformacion() {
        return "Nombre" + getNombre() + ", Edad :" + getEdad() + ", numero de empleado" + numeroEmpleado;
    }
    
}
